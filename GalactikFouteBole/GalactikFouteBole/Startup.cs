﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GalactikFouteBole.Startup))]
namespace GalactikFouteBole
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
