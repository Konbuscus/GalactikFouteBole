﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GalactikFouteBole.Helpers;
using GalactikFouteBole.Models;
using GalactikFouteBole.Globals;

namespace GalactikFouteBole.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Welcome w = new Welcome();
            int blogBlock = 5;
            int CompetBlock = 1;
            int MatchBlock = 7;
            w.blogBlock = BlogHelper.GetListOfBlogs().OrderByDescending(x => x.DayWritten).Take(blogBlock).ToList();
            w.CompetEnCours = CompetHelper.getCompetById(CompetBlock);
            w.matchBlock = TeamHelper.GetMatchByCompetId(CompetBlock).Take(MatchBlock).ToList();
            return View(w);
        }

        public ActionResult Connect()
        {
            return View();
        }

        public ActionResult Disconnect()
        {
            if (Variables.CurrentUser != null)
                Variables.CurrentUser = null;
            return View("Index");
        }

        [HttpPost]
        public ActionResult Logging(string username, string password)
        {
            //On récupère les identifiants, et on les compare en base
            User user = UsersHelper.GetUsersDetails(username);
            //Si les identifiants sont valides, on revoie la vue d'espace personnel
            if(user != null)
            {
                if (user.Email == username && user.Password == password)
                {
                    Variables.CurrentUser = user;
                    return Json(Url.Action("Index", "Home"));
                }
            }

            //Sinon on reload la page avec un erreur
            return View("Error");
        }

        
    }
}