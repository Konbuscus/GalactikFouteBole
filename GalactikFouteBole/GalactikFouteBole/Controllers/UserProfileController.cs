﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GalactikFouteBole.Models;
using GalactikFouteBole.Globals;
using GalactikFouteBole.Helpers;

namespace GalactikFouteBole.Controllers
{
    public class UserProfileController : Controller
    {
        // GET: UserProfile
        public ActionResult Index()
        {
            User currentUser = Variables.CurrentUser;
            if (currentUser != null)
            {
                System.Diagnostics.Debug.WriteLine("Current user page profil: " + currentUser.Name);
                return View(BlogHelper.GetInvalidComments());
            }
            else
                return View("~/Views/Home/Connect.cshtml");
        }

        [HttpPost]
        public ActionResult UpdatedUserFields(User user)
        {
            UsersHelper.UpdateUserDetails(user);
            return View();
        }

        [HttpPost]
        public ActionResult ValidateComs(int[] comsToValidate)
        {
            BlogHelper.ValidateComments(comsToValidate);
            return View(BlogHelper.GetInvalidComments());
        }
    }
}