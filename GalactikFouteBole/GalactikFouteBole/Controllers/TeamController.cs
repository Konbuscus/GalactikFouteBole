﻿using GalactikFouteBole.Helpers;
using GalactikFouteBole.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GalactikFouteBole.Controllers
{
    public class TeamController : Controller
    {
        // GET: Team
        /// <summary>
        /// Retourne la liste des équipes de foot sur une seule page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<Team> teams = TeamHelper.GetTeamList();
            foreach(Team team in teams)
            {
                team.ImgTeam = Url.Content("~/Content/ImgTeam/" + team.TeamId + ".png");
            }
            return View(teams);
        }

        /// <summary>
        /// Retourne une équipe dans son détail (nouvelle vue)
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public ActionResult TeamDetails(int idTeam)
        {
            Team detailedTeam = TeamHelper.GetTeamById(idTeam);
            detailedTeam.ImgTeam = Url.Content("~/Content/ImgTeam/" + detailedTeam.TeamId + ".png");
           
            return View(detailedTeam);
        }

        [HttpGet]
        public ActionResult GetCompetTeams(int idTeam)
        {
            //Récupérer la liste de toutes les compétitions, puis afficher un bouton par competition avec nom et ID
            List<Competition> CompetList = CompetHelper.GetCompetListForATeam(idTeam);
            ViewData["TeamId"] = idTeam;
            
            return PartialView("~/Views/Team/CompetParticipated.cshtml", CompetList);
        }

        /// <summary>
        /// Récupère le graph statistique d'une équipe pour la compétition réalisée.
        /// </summary>
        /// <param name="idTeam"></param>
        /// <param name="idCompet"></param>
        /// <returns></returns>
        public ActionResult GetTeamStatsOnCompet(int idTeam, int idCompet)
        {

            //Récupération des stats et affichage
            Stats statsToDisplay = TeamHelper.GetStatsOfTeamForACompet(idTeam, idCompet);
            Competition c = CompetHelper.getCompetById(idCompet);
            //ViewData["idCompet"] = c.Id;
            ViewData["idTeam"] = idTeam;
            ViewData["TirCadre"] = statsToDisplay.tirCadré.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["Possession"] = statsToDisplay.PossessionBalles.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["Passes"] = statsToDisplay.PassesSuccess.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["Dududu"] = statsToDisplay.DuelAerienWon.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["CartonJaune"] = statsToDisplay.CartonJaunes;
            ViewData["CartonRouge"] = statsToDisplay.CartonsRouges;
            return PartialView("~/Views/Team/Stats.cshtml", statsToDisplay);
        }

        public ActionResult EditTeamDetails(int idTeam)
        {
            Team team = TeamHelper.GetTeamById(idTeam);
            return View(team);
        }

        [HttpPost]
        public ActionResult EditingTeamDetails(int idTeam, string TeamName, string description)
        {
           bool isAdmin =  Globals.Variables.CurrentUser.IsAdmin;

            if(isAdmin)
            {
                Team team = TeamHelper.GetTeamById(idTeam);
                //Edition de l'équipe via son ID
                Team editedTeam = TeamHelper.EditTeamDetails(idTeam, TeamName, description);
                ViewData["teamName"] = team.TeamName;
                return View(editedTeam);
            }
            return null;
        }
    }
}