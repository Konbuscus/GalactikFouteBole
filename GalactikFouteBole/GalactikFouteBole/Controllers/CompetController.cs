﻿using GalactikFouteBole.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GalactikFouteBole.Models
{
    public class CompetController : Controller
    {
        // GET: Compet
        /// <summary>
        /// Récupère la liste de toutes les compétitions existantes
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<Competition> CompetitionList = CompetHelper.GetTeamList();
            foreach(var compet in CompetitionList)
            {
                compet.ImageCompet = Url.Content("~/Content/ImgCompet/" + compet.Id + ".png");
                foreach(var team in compet.ListTeamParticipate)
                {
                    team.ImgTeam = Url.Content("~/Content/ImgTeam/" + team.TeamId + ".png");
                }
            }
            return View(CompetitionList);
        }

        /// <summary>
        /// Récupère les statistiques  moyennes d'une compétition (moyenne de toutes les stats sur chaque stats)
        /// </summary>
        /// <param name="idCompet">id de la compéition</param>
        /// <returns></returns>
        public ActionResult GetStatsForCompet(int idCompet)
        {
            Stats StatList = CompetHelper.GetStatsOnCompet(idCompet);
            Competition c = CompetHelper.getCompetById(idCompet);
            ViewData["NomCompet"] = c.CompetitionName;
            ViewData["TirCadre"] = StatList.tirCadré.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["Possession"] = StatList.PossessionBalles.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["Passes"] = StatList.PassesSuccess.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["Dududu"] = StatList.DuelAerienWon.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            ViewData["CartonJaune"] = StatList.CartonJaunes;
            ViewData["CartonRouge"] = StatList.CartonsRouges;

            return View("~/Views/Compet/GetStatsForCompet.cshtml", StatList);
        }

        /// <summary>
        /// Récupère la liste des matchs d'une compétition  (joueurs également)
        /// </summary>
        /// <param name="idCompet"></param>
        /// <returns></returns>
        public ActionResult GetMatchOnCompet(int idCompet)
        {
            //Récupère la compététion.
            Competition c = CompetHelper.getCompetById(idCompet);
            c.CompetMatchs = TeamHelper.GetMatchByCompetId(idCompet).OrderByDescending(x => x.IdMatch).ToList();
            c.ImageCompet = Url.Content("~/Content/ImgCompet/" + idCompet + ".png");
            foreach(var d in c.CompetMatchs)
            {
                foreach(var e in d.Equipes)
                {
                    e.ImgTeam = Url.Content("~/Content/ImgTeam/" + e.TeamId + ".png");
                }
            }
            return View(c);

        }

        public ActionResult AddMatchToCompet()
        {
            ViewData["ListCompet"] = CompetHelper.GetTeamList();
            ViewData["ListTeam"] = TeamHelper.GetTeamList();

            return View();
        }
        /// <summary>
        /// Ajout d'un match pour une compétition
        /// </summary>
        /// <param name="idCompet"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddingMatchToCompet(Match m)
        {
            Competition c = CompetHelper.getCompetById(m.IdChampionnat);
            CompetHelper.AddMatchToCompetition(m);
            return RedirectToAction("GetMatchOnCompet", "Compet", new { idCompet = m.IdChampionnat });
        }
    }
}