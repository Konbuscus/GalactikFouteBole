﻿using GalactikFouteBole.Helpers;
using GalactikFouteBole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GalactikFouteBole.Controllers
{
    public class BlogsController : Controller
    {
        // GET: Blogs
        public ActionResult Index()
        {
            //On veut retourner tous les blogs disponibles en base
            List<Blog> blogs = BlogHelper.GetListOfBlogs().OrderByDescending(x=> x.DayWritten).ToList();
            return View(blogs);
        }

        /// <summary>
        /// Accès au détail du blog TODO: Ajouter les commentaires
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(int id)
        {
            Blog blog = BlogHelper.GetBlogById(id);
            return View(blog);
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult InsertBlog(string blogtitle, string blogcontent)
        {
            Blog blog = BlogHelper.InsertBlog(blogtitle, blogcontent);
            return RedirectToAction("Index","Blogs");
        }

        public ActionResult EditBlog(int id)
        {
            Blog blog = BlogHelper.GetBlogById(id);
            return View(blog);
        }

        public ActionResult DeleteBlog(int blogId)
        {
            BlogHelper.DeleteBlog(blogId);
            return RedirectToAction("Index","Blogs");
        }

        public ActionResult EditingBlog(int id, string blogtitle, string blogcontent)
        {
            Blog MagicBlog = new Blog();
            MagicBlog.Subject = blogtitle;
            MagicBlog.Message = blogcontent;
            MagicBlog.Id = id;
            BlogHelper.EditBlog(MagicBlog);
            return RedirectToAction("Index","Blogs");
        }

        // Vue avec formulaire d'ajout de commentaire
        public ActionResult AddCom(int blogId)
        {
            return View();
        }

        public ActionResult AddComment(string body, int authorId, int blogId)
        {
            Comment newCom = new Comment
            {
                Author_id = authorId,
                Blog_id = blogId,
                Body = body,
                HasBeenAccepted = false
            };

            BlogHelper.AddComment(newCom);

            return RedirectToAction("Index", "Blogs");
        }

        //[HttpPost]
        public ActionResult EditCom(int id)
        {
            Comment com = BlogHelper.GetCommentById(id);
            return View(com);
        }

        public ActionResult EditComment(string body, int comId)
        {
            Comment editedCom = new Comment
            {
                Id = comId,
                Body = body
            };
            BlogHelper.EditComment(editedCom);
            return RedirectToAction("Index", "Blogs");
        }

        public ActionResult DelCom(int comId)
        {
            BlogHelper.DelCom(comId);
            return RedirectToAction("Index", "Blogs");
        }
    }
}