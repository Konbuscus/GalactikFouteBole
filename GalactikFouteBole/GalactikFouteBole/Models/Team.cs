﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    public class Team
    {
        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public string TeamShortName { get; set; }

        public string Country { get; set; }

        public string ImgTeam { get; set; }

        public string description { get; set; }

    }
}