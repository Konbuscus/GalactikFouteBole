﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    public class Match
    {
        public int IdMatch { get; set; }
        public string NomMatch { get; set; }
        public int IdEquipe1 { get; set; }
        public int IdEquipe2 { get; set; }
        public int NombreButEquipe1 { get; set; }
        public int NombreButEquipe2 { get; set; }
        public int IdChampionnat { get; set; }
        public List<Team> Equipes { get; set; }
    }
}