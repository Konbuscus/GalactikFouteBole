﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public bool HasBeenAccepted { get; set; }
        public int Blog_id { get; set; }
        public string Author { get; set; }
        public int Author_id { get; set; }
    }
}