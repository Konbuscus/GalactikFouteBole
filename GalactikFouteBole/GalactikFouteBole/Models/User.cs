﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    public class User
    {
        // TOOD: Required
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public int IdCivility { get; set; } // FK
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Address { get; set; }
        public string AddressComplement { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public int Phone { get; set; }
        public int SubscriberNumber { get; set; }
        public DateTime Birthday { get; set; }
        public int IdCountry { get; set; } // FK
        public string State { get; set; }
        public bool NewsLetterSub { get; set; }
        public bool OffersSub { get; set; }
        public bool IsAdmin { get; set; }
    }
}