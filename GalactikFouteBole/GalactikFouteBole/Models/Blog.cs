﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    /// <summary>
    /// Model du blog
    /// </summary>
    public  class Blog

    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime DayWritten { get; set; }
        public int AuthorId { get; set; }
        public List<Comment> CommentsOnBlog { get; set; }
    }
}