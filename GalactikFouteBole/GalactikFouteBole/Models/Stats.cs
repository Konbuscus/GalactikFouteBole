﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    public class Stats
    {

        public int idStats { get; set; }

        public double tirCadré { get; set; }

        public int CartonJaunes { get; set; }

        public int CartonsRouges { get; set; }

        public double PossessionBalles { get; set; }

        public double PassesSuccess { get; set; }

        public double DuelAerienWon { get; set; }


    }

}