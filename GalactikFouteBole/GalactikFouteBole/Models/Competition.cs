﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Models
{
    public class Competition
    {
        public int Id { get; set; }

        public string CompetitionName { get; set; }

        public List<Team> ListTeamParticipate { get; set; }

        public DateTime DateDebut { get; set; }

        public DateTime DateFin { get; set; }

        public string ImageCompet { get; set; }

        public List<Match> CompetMatchs { get; set; }

        //TODO: Liste des matchs
    }
}