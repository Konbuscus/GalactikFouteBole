﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GalactikFouteBole.App_Start;
using MySql.Data.MySqlClient;
using System.Data;
using GalactikFouteBole.Models;
using static GalactikFouteBole.Helpers.Utils;

namespace GalactikFouteBole.Helpers
{
    public class UsersHelper
    {
        public static User GetUsersDetails(string login)
        {
            string request = string.Format("SELECT * FROM User WHERE user.Email = '{0}'", login);
            DbHelper DB = new DbHelper();           
            MySqlCommand command = new MySqlCommand(request);
            DataTable userData = DB.ExecuteSql(command);
            User user = new User();

            if (userData != null)
            {
                foreach (DataRow row in userData.Rows)
                {
                    user.Id = DbHelper.GetMapping(row, "Id", -1);
                    user.UserName = DbHelper.GetMapping(row, "UserName", string.Empty);
                    user.Email = DbHelper.GetMapping(row, "Email", string.Empty);
                    user.Password = DbHelper.GetMapping(row, "Password", string.Empty);
                    user.IdCivility = DbHelper.GetMapping(row, "IdCivility", -1);
                    user.Name = DbHelper.GetMapping(row, "Name", string.Empty);
                    user.FirstName = DbHelper.GetMapping(row, "FirstName", string.Empty);
                    user.Address = DbHelper.GetMapping(row, "Address", string.Empty);
                    user.AddressComplement = DbHelper.GetMapping(row, "AddressComplement", string.Empty);
                    user.PostalCode = DbHelper.GetMapping(row, "PostalCode", -1);
                    user.City = DbHelper.GetMapping(row, "City", string.Empty);
                    user.Phone = DbHelper.GetMapping(row, "Phone", -1);
                    user.SubscriberNumber = DbHelper.GetMapping(row, "SubscriberNumber", -1);
                    user.Birthday = DbHelper.GetMapping(row, "Birthday", DateTime.MinValue);
                    user.IdCountry = DbHelper.GetMapping(row, "IdCountry", -1);
                    user.State = DbHelper.GetMapping(row, "State", string.Empty);
                    user.NewsLetterSub = DbHelper.GetMapping(row, "NewsLetterSub", false);
                    user.OffersSub = DbHelper.GetMapping(row, "OffersSub", false);
                    user.IsAdmin = Convert.ToBoolean(DbHelper.GetMapping<Int32>(row, "IsAdmin", 0));
                }
            }

            return user;
        }

        public static void UpdateUserDetails(User updatedUser)
        {
            User currentUser = GalactikFouteBole.Globals.Variables.CurrentUser;
            if (currentUser == null)
            {
                System.Diagnostics.Debug.WriteLine("%%%%%%\nL'utilisateur n'est pas connecté, impossible d'updater ses infos en base.%%%%%%\n");
                return;
            }

            DbHelper db = new DbHelper();
            string sql = $"UPDATE user SET UserName='{updatedUser.UserName}', Email='{updatedUser.Email}', Password='{updatedUser.Password}', IdCivility={updatedUser.IdCivility}, Name='{updatedUser.Name}', FirstName='{updatedUser.FirstName}', Address='{updatedUser.Address}', AddressComplement='{updatedUser.AddressComplement}', PostalCode={updatedUser.PostalCode}, City='{updatedUser.City}', Phone={updatedUser.Phone}, SubscriberNumber={updatedUser.SubscriberNumber}, Birthday='{updatedUser.Birthday}', IdCountry={updatedUser.IdCountry}, State='{updatedUser.State}', NewsLetterSub={updatedUser.NewsLetterSub}, OffersSub={updatedUser.OffersSub}, IsAdmin={updatedUser.IsAdmin} WHERE Id = {currentUser.Id}";
            MySqlCommand cmd = new MySqlCommand(sql);
            db.ExecuteSql(cmd);
        }

        public static User GetUserByID(int userID)
        {
            string sql = "SELECT * FROM user WHERE user.Id = " + userID;
            MySqlCommand cmd = new MySqlCommand(sql);
            DbHelper hlp = new DbHelper();
            DataTable usersTable = hlp.ExecuteSql(cmd);
            if (usersTable != null)
            {
                User user = new User();
                foreach (DataRow row in usersTable.Rows)
                {
                    user.Id = DbHelper.GetMapping(row, "Id", -1);
                    user.UserName = DbHelper.GetMapping(row, "UserName", string.Empty);
                    user.Email = DbHelper.GetMapping(row, "Email", string.Empty);
                    user.Password = DbHelper.GetMapping(row, "Password", string.Empty);
                    user.IdCivility = DbHelper.GetMapping(row, "IdCivility", -1);
                    user.Name = DbHelper.GetMapping(row, "Name", string.Empty);
                    user.FirstName = DbHelper.GetMapping(row, "FirstName", string.Empty);
                    user.Address = DbHelper.GetMapping(row, "Address", string.Empty);
                    user.AddressComplement = DbHelper.GetMapping(row, "AddressComplement", string.Empty);
                    user.PostalCode = DbHelper.GetMapping(row, "PostalCode", -1);
                    user.City = DbHelper.GetMapping(row, "City", string.Empty);
                    user.Phone = DbHelper.GetMapping(row, "Phone", -1);
                    user.SubscriberNumber = DbHelper.GetMapping(row, "SubscriberNumber", -1);
                    user.Birthday = DbHelper.GetMapping(row, "Birthday", DateTime.MinValue);
                    user.IdCountry = DbHelper.GetMapping(row, "IdCountry", -1);
                    user.State = DbHelper.GetMapping(row, "State", string.Empty);
                    user.NewsLetterSub = DbHelper.GetMapping(row, "NewsLetterSub", false);
                    user.OffersSub = DbHelper.GetMapping(row, "OffersSub", false);
                    user.IsAdmin = Convert.ToBoolean(DbHelper.GetMapping<Int32>(row, "IsAdmin", 0));
                    break; // Devrait pas y'avoir + d'une iteration de tte facon.
                }
                return user;
            }
            return null;
        }
    }
}