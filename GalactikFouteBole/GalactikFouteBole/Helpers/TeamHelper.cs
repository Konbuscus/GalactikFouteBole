﻿using GalactikFouteBole.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Helpers
{
    public class TeamHelper
    {
        /// <summary>
        /// Récupère la liste des équipes de la ligue 1 datant 2016
        /// </summary>
        /// <returns></returns>
        public static List<Team> GetTeamList()
        {
            string request = string.Format("SELECT * FROM TEAM");
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable listTeam = DBContext.ExecuteSql(command);
            List<Team> teams = new List<Team>();

            if (listTeam != null)
            {
                foreach (DataRow row in listTeam.Rows)
                {
                    Team team = new Team()
                    {
                        TeamId = DbHelper.GetMapping(row, "Id", -1),
                        TeamName = DbHelper.GetMapping(row, "Name", string.Empty),
                        TeamShortName = DbHelper.GetMapping(row, "ShortName", string.Empty),
                        ImgTeam = DbHelper.GetMapping(row, "CrestUrl", string.Empty)
                    };
                    teams.Add(team);
                }
                return teams;
            }
            return teams;
        }

        /// <summary>
        /// Récupère les données d'une équipe pour son détail
        /// </summary>
        /// <param name="idTeam"></param>
        /// <returns></returns>
        public static Team GetTeamById(decimal idTeam)
        {
            string request = string.Format("SELECT * FROM TEAM WHERE Id = {0}",idTeam);
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable team = DBContext.ExecuteSql(command);
            if(team != null)
            {
                var ElFamosoTeam = new Team();
                foreach (DataRow row in team.Rows)
                {
                    ElFamosoTeam.TeamId = DbHelper.GetMapping(row, "Id", -1);
                    ElFamosoTeam.TeamName = DbHelper.GetMapping(row, "Name", string.Empty);
                    ElFamosoTeam.description = DbHelper.GetMapping(row, "Description", string.Empty);
                }
                return ElFamosoTeam;
            }
            return null;
        }

        //Modifie le nom d'une équipe et sa description en base
        public static Team EditTeamDetails(int idTeam, string TeamName, string description)
        {
            string request = string.Format("UPDATE TEAM SET TEAM.NAME = '{0}', TEAM.DESCRIPTION = '{1}' WHERE TEAM.Id = {2}", TeamName, description, idTeam);
            DbHelper DBcontext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable team = DBcontext.ExecuteSql(command);
            Team AncientTeam = GetTeamById(idTeam);
            AncientTeam.ImgTeam = "~/Content/ImgTeam/" + idTeam + ".png";
            Team editedTeam = new Team()
            {
                TeamId = idTeam,
                TeamName = TeamName,
                description = description,
                ImgTeam = AncientTeam.ImgTeam
            };
            return editedTeam;
        }

        /// <summary>
        /// Récupère les statistiques d'une équipe pour une compétition
        /// </summary>
        /// <param name="idTeam"></param>
        /// <param name="idCompet"></param>
        /// <returns></returns>
        public static Stats GetStatsOfTeamForACompet(int idTeam, int idCompet)
        {
            string request =
                string.Format(
                    "SELECT * FROM STATS LEFT JOIN lier_stats_equipe_championnat ON STATS.ID = lier_stats_equipe_championnat.IDLink " +
                    "WHERE lier_stats_equipe_championnat.IDEquipe = {0} AND lier_stats_equipe_championnat.IDChampionnat = {1}",
                    idTeam, idCompet);

            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable statistiques = DBContext.ExecuteSql(command);
            if (statistiques != null)
            {
                Stats StatOfTeam = new Stats();

                foreach (DataRow row in statistiques.Rows)
                {
                    StatOfTeam.tirCadré = DbHelper.GetMapping(row, "TirCadre", (float)0);
                    StatOfTeam.CartonJaunes = DbHelper.GetMapping(row, "CartonsJaunes", 0);
                    StatOfTeam.CartonsRouges = DbHelper.GetMapping(row, "CartonsRouges", 0);
                    StatOfTeam.PossessionBalles = DbHelper.GetMapping(row, "PossessionDeBalle", (double)0);
                    StatOfTeam.PassesSuccess = DbHelper.GetMapping(row, "PassesReussies", (double)0);
                    StatOfTeam.DuelAerienWon = DbHelper.GetMapping(row, "DuelAerien", (float)0);
                }
                return StatOfTeam;
            }
            return null;
        }

        /// <summary>
        /// Récupère la liste des équipes qui participent à une compétition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Team> GetTeamByCompetitionId(decimal id)
        {
            string request = string.Format("SELECT DISTINCT * FROM TEAM " +
                                            "LEFT JOIN lier_equipe_competition ON team.Id = lier_equipe_competition.IDTEAM " +
                                            "WHERE lier_equipe_competition.IDCOMPET = {0}", id);
            DbHelper DBcontext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable teams = DBcontext.ExecuteSql(command);
            List<Team> ListTeam = new List<Team>();

            if (teams != null)
            {
                foreach (DataRow row in teams.Rows)
                {
                    Team team = new Team()
                    {
                        TeamId = DbHelper.GetMapping(row, "Id", -1),
                        TeamName = DbHelper.GetMapping(row, "Name", string.Empty),
                        TeamShortName = DbHelper.GetMapping(row, "ShortName", string.Empty),
                        ImgTeam = DbHelper.GetMapping(row, "CrestUrl", string.Empty)
                    };
                   
                    ListTeam.Add(team);
                }
                return ListTeam;
            }
            return ListTeam;
        }

        /// <summary>
        /// Récupère la liste des matchs de la compétition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Match> GetMatchByCompetId(int id)
        {
            string request = string.Format("SELECT DISTINCT * FROM `match` " +
                                            "LEFT JOIN competition ON `match`.IdChampionnat = competition.Id " +
                                            "WHERE `match`.IdChampionnat = {0} ", id);
            DbHelper DBcontext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable matchs = DBcontext.ExecuteSql(command);
            List<Match> CompetMatch = new List<Match>();

            if(matchs != null)
            {
                foreach(DataRow row in matchs.Rows)
                {
                    Match match = new Match();
                    match.IdMatch = DbHelper.GetMapping(row, "IdMatch", 0);
                    match.NomMatch = DbHelper.GetMapping(row, "NomMatch", string.Empty);
                    match.IdEquipe1 = DbHelper.GetMapping(row, "IdEquipe1", 0);
                    match.IdEquipe2 = DbHelper.GetMapping(row, "IdEquipe2", 0);
                    match.NombreButEquipe1 = DbHelper.GetMapping(row, "NombreButEquipe1", 0);
                    match.NombreButEquipe2 = DbHelper.GetMapping(row, "NombreButEquipe2", 0);
                    match.IdChampionnat = DbHelper.GetMapping(row, "IdChampionnat", 0);
                    match.Equipes = new List<Team>();
                    match.Equipes = MatchHelper.GetTeamsInAMatch(match.IdMatch);
                    CompetMatch.Add(match);
                }

                return CompetMatch;
            }
            return CompetMatch;
        }


    }
}