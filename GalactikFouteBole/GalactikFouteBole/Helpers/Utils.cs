﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Helpers
{
    public static class Utils
    {
        public static T iif<T>(bool cond, T trueValue, T falseValue)
        {
            return (cond) ? trueValue : falseValue;
        }
    }
}