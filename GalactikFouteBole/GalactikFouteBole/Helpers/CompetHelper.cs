﻿using GalactikFouteBole.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Helpers
{
    public  class CompetHelper
    {
        /// <summary>
        /// Récupère la liste compéitions
        /// </summary>
        /// <returns></returns>
        public static List<Competition> GetTeamList()
        {
            string request = string.Format("SELECT * FROM COMPETITION");
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable listCompet = DBContext.ExecuteSql(command);
            List<Competition> competitions = new List<Competition>();

            if (listCompet != null)
            {
                foreach (DataRow row in listCompet.Rows)
                {
                    Competition competition = new Competition()
                    {
                        Id = DbHelper.GetMapping(row, "Id", -1),
                        CompetitionName = DbHelper.GetMapping(row, "Name", string.Empty),
                        DateDebut = DbHelper.GetMapping(row, "DateDebut", DateTime.Now),
                        DateFin = DbHelper.GetMapping(row, "DateFin", DateTime.Now),
                       
                    };
                    competition.ListTeamParticipate = TeamHelper.GetTeamByCompetitionId(competition.Id);
                    competition.CompetMatchs = TeamHelper.GetMatchByCompetId(competition.Id);
                    competitions.Add(competition);
                }
                return competitions;
            }
            return competitions;
        }

        public static Competition getCompetById(int id)
        {
            Competition c = new Competition();
            string request = string.Format("SELECT * FROM COMPETITION WHERE Id = {0}", id);
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable name = DBContext.ExecuteSql(command);
            foreach (DataRow r in name.Rows)
            {
                c.CompetitionName = DbHelper.GetMapping<string>(r, "Name", string.Empty);
                c.DateDebut = DbHelper.GetMapping(r, "DateDebut", DateTime.Now);
                c.DateFin = DbHelper.GetMapping(r, "DateFin", DateTime.Now);
                c.Id = id;
                break;
            }
            return c;
        }

        /// <summary>
        /// Récupère les stats moyennes d'une compéition
        /// </summary>
        /// <param name="idCompet"></param>
        /// <returns></returns>
        public static Stats GetStatsOnCompet(int idCompet)
        {
            
           
            string request = string.Format("SELECT AVG(TirCadre) as TirCadre, AVG(Cast(CartonsJaunes as DOUBLE)) as CartonsJaunes, AVG(Cast(CartonsRouges as DOUBLE)) as CartonsRouges, AVG(PossessionDeBalle) as PossessionDeBalle , AVG(PassesReussies) as PassesReussies, AVG(DuelAerien) as DuelAerien, Id FROM STATS LEFT JOIN lier_stats_equipe_championnat lsec ON STATS.ID = lsec.IDSTATS " +
                                           "WHERE IDCHAMPIONNAT = {0}", idCompet);
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable Stats = DBContext.ExecuteSql(command);
            if(Stats != null)
            {
                Stats s = new Stats();
                foreach (DataRow row in Stats.Rows)
                {
                    s.tirCadré = DbHelper.GetMapping(row, "TirCadre", (double)0);
                    var asd = DbHelper.GetMapping<double>(row, "CartonsJaunes", default(double));
                    s.CartonJaunes = Convert.ToInt32(asd);
                    s.CartonsRouges = Convert.ToInt32(DbHelper.GetMapping<double>(row, "CartonsRouges", default(double)));
                    s.PossessionBalles = DbHelper.GetMapping(row, "PossessionDeBalle", (double)0);
                    s.PassesSuccess = DbHelper.GetMapping(row, "PassesReussies", (double)0);
                    s.DuelAerienWon = DbHelper.GetMapping(row, "DuelAerien", (double)0);
                    s.idStats = DbHelper.GetMapping(row, "Id", 0);
                }
                return s;
            }
            return null;
        }
        /// <summary>
        /// Obtient la liste des compétitions auxquels une équipe à participerD:\GalactikFouteBole\GalactikFouteBole\GalactikFouteBole\App_Start\IdentityConfig.cs
        /// </summary>
        /// <param name="idTeam">id de l'équipe</param>
        /// <returns></returns>
        public static List<Competition> GetCompetListForATeam(decimal idTeam)
        {
            List<Competition> CompetList = new List<Competition>();
             string request = string.Format("SELECT * FROM COMPETITION LEFT JOIN LIER_EQUIPE_COMPETITION ON COMPETITION.Id = LIER_EQUIPE_COMPETITION.IDCOMPET WHERE IDTEAM = {0}",idTeam);
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable Compets = DBContext.ExecuteSql(command);
            if (Compets != null)
            {
                foreach (DataRow row in Compets.Rows)
                {

                    Competition c = new Competition();
                    c.CompetitionName = DbHelper.GetMapping(row, "Name", String.Empty);
                    c.Id = DbHelper.GetMapping(row, "Id", 0);
                    c.DateDebut = DbHelper.GetMapping(row, "DateDebut", DateTime.MinValue);
                    c.DateFin = DbHelper.GetMapping(row, "DateFin", DateTime.MinValue);
                    CompetList.Add(c);

                }
                return CompetList;
            }
            return null;
        }

        /// <summary>
        /// Ajoute un match par rapport à un championnat
        /// </summary>
        /// <param name="m"></param>
        public static void AddMatchToCompetition(Match m)
        {
            string request = string.Format("INSERT INTO `Match`  (NomMatch,IdEquipe1,IdEquipe2,NombreButEquipe1,NombreButEquipe2,IdChampionnat) VALUES ('{0}', {1}, {2}, {3}, {4}, {5})", m.NomMatch, m.IdEquipe1, m.IdEquipe2, m.NombreButEquipe1, m.NombreButEquipe2, m.IdChampionnat);
            DbHelper.lazyExecuteScalar(new MySqlCommand(request));
        }
    }
}