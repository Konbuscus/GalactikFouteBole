﻿using GalactikFouteBole.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Helpers
{
    public class BlogHelper
    {
        /// <summary>
        /// Va retourner la vue avec tous les blogs disponibles en base
        /// </summary>
        /// <returns></returns>
        public static List<Blog> GetListOfBlogs()
        {
            string request = string.Format("SELECT * FROM BLOG");
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable listBlog = DBContext.ExecuteSql(command);
            List<Blog> blogs = new List<Blog>();

            if (listBlog != null)
            {
                foreach (DataRow row in listBlog.Rows)
                {
                    Blog blog = new Blog();
                    blog.Id = DbHelper.GetMapping(row, "Id", -1);
                    blog.Subject = DbHelper.GetMapping(row, "Subject", string.Empty);
                    blog.Message = DbHelper.GetMapping(row, "Message", string.Empty);
                    blog.DayWritten = DbHelper.GetMapping(row, "DayWritten", DateTime.MinValue);
                    blogs.Add(blog);
                    //TODO: Inclure les commentaires liées aux blogs dans la requête plus haut
                    //blog.CommentsOnBlog = DbHelper.GetMapping(row,"Comments", )
                }
                return blogs;
            }
            return blogs;
        }

        internal static void ValidateComments(int[] comsToValidate)
        {
            string sql = "UPDATE comments SET HasBeenAccepted=1 WHERE ";
            for (int i=0; i<comsToValidate.Length; i++)
            {
                sql += " comments.Id=" + comsToValidate[i] + " AND";
            }
            sql = sql.Remove(sql.Length - 3); //On vire le AND
            DbHelper.lazyExecuteScalar(new MySqlCommand(sql));
        }

        public static Blog GetBlogById(decimal id)
        {
            string request = string.Format("SELECT * FROM BLOG WHERE Id = {0}",id);
            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable blog = DBContext.ExecuteSql(command);

            if(blog != null)
            {
                Blog elFamosoBlog = new Blog();
                foreach (DataRow row in blog.Rows)
                {
                    elFamosoBlog.Id = DbHelper.GetMapping(row, "Id", -1);
                    elFamosoBlog.Subject = DbHelper.GetMapping(row, "Subject", string.Empty);
                    elFamosoBlog.Message = DbHelper.GetMapping(row, "Message", string.Empty);
                    elFamosoBlog.DayWritten = DbHelper.GetMapping(row, "DayWritten", DateTime.MinValue);
                    elFamosoBlog.CommentsOnBlog = BlogHelper.GetBlogComments(elFamosoBlog.Id);
                }
                return elFamosoBlog;
            }
            return null;
        }

        public static Blog InsertBlog(string title,  string content)
        {
            var currentDate = DateTime.Now;
            var request = $"INSERT INTO `blog` (`Subject`, `Message`, `DayWritten`) VALUES (@title, @content, @currentdate); SELECT LAST_INSERT_ID();";
            var DbInstance = new DbHelper();
            var cmd = new MySqlCommand(request);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@content", content);
            cmd.Parameters.AddWithValue("@currentdate", currentDate);

            var NewBlogId = DbInstance.ExecuteScalar(cmd);

            //if (NewBlogId > 0 || NewBlogId != DbHelper.ERROR_RETURN)
            //{
            //    var NewBlog = BlogHelper.GetBlogById(NewBlogId);
            //    if (NewBlog != null)
            //        return NewBlog;
            //    else
            //        return null;
            //}

            return null;
        }

        internal static void DelCom(int comId)
        {
            string sql = "DELETE FROM comments WHERE comments.Id = " + comId;
            DbHelper.lazyExecuteScalar(new MySqlCommand(sql));
        }

        /// <summary>
        /// Permet l'édition d'un blog
        /// </summary>
        /// <param name="blog"></param>
        /// <returns></returns>
        public static Blog EditBlog(Blog blog)
        {
            blog.DayWritten = DateTime.Now;
            var request = string.Format("UPDATE BLOG SET Subject = '{0}', Message = '{1}', DayWritten = '{2}' WHERE BLOG.Id = {3}",blog.Subject, blog.Message, blog.DayWritten.ToString("yyyy/MM/dd : hh:mm:ss"), blog.Id);
            var cmd = new MySqlCommand(request);
            var DbInstance = new DbHelper();
            var finalBlog = DbInstance.ExecuteScalar(cmd);
            return blog;
        }

        /// <summary>
        /// Permet la suppression d'un blog
        /// </summary>
        /// <param name="blog"></param>
        /// <returns></returns>
        public static Blog DeleteBlog(int blogId)
        {
            var request = string.Format("DELETE FROM BLOG WHERE BLOG.ID = {0}", blogId);
            var cmd = new MySqlCommand(request);
            var DbInstance = new DbHelper();
            var finalBlog = DbInstance.ExecuteScalar(cmd);
            return null;
        }

        public static List<Comment> GetBlogComments(int blogId)
        {
            List<Comment> commentsList = new List<Comment>();
            string sql = @"SELECT 
	                            C.Id,
                                C.Body,
                                C.HasBeenAccepted,
                                C.Blog_id,
                                C.User_id AS Author_id
                            FROM galaktikfoutebole.comments C
                            LEFT JOIN blog B ON B.Id = C.Blog_id
                            WHERE B.Id = " + blogId;
            MySqlCommand cmd = new MySqlCommand(sql);
            DbHelper hlp = new DbHelper();
            DataTable comments = hlp.ExecuteSql(cmd);
            if (comments != null)
            {
                foreach (DataRow row in comments.Rows)
                {
                    commentsList.Add(new Comment
                    {
                        Id = DbHelper.GetMapping(row, "Id", -1),
                        Body = DbHelper.GetMapping(row, "Body", string.Empty),
                        HasBeenAccepted = Convert.ToBoolean(DbHelper.GetMapping(row, "HasBeenAccepted", 0)),
                        Blog_id = DbHelper.GetMapping(row, "Blog_id", -1),
                        Author = UsersHelper.GetUserByID(DbHelper.GetMapping(row, "Author_id", -1)).Name,
                        Author_id = DbHelper.GetMapping(row, "Author_id", -1)
                    });
                }
                return commentsList;
            }
            return null;
        }

        public static int AddComment(Comment newCom)
        {
            string request = $"INSERT INTO comments (Body, HasBeenAccepted, User_id, Blog_id) VALUES (@body, 0, @user_id, @blog_id); SELECT LAST_INSERT_ID();";
            var DbInstance = new DbHelper();
            var cmd = new MySqlCommand(request);
            cmd.Parameters.AddWithValue("@body", newCom.Body);
            cmd.Parameters.AddWithValue("@user_id", newCom.Author_id);
            cmd.Parameters.AddWithValue("@blog_id", newCom.Blog_id);

            int newCommentId = DbInstance.ExecuteScalar(cmd);
            return newCommentId;
        }

        public static Comment GetCommentById(int id)
        {
            string sql = "SELECT * FROM comments WHERE comments.Id = " + id;
            DbHelper hlp = new DbHelper();
            MySqlCommand cmd = new MySqlCommand(sql);
            DataTable table = hlp.ExecuteSql(cmd);
            if (table == null)
            {
                return null;
            }
            foreach (DataRow row in table.Rows)
            {
                Comment newComment = new Comment
                {
                    Id = DbHelper.GetMapping(row, "Id", -1),
                    Body = DbHelper.GetMapping(row, "Body", string.Empty),
                    HasBeenAccepted = Convert.ToBoolean(DbHelper.GetMapping(row, "HasBeenAccepted", 0)),
                    Blog_id = DbHelper.GetMapping(row, "Blog_id", -1),
                    Author = UsersHelper.GetUserByID(DbHelper.GetMapping(row, "User_id", -1)).Name,
                    Author_id = DbHelper.GetMapping(row, "User_id", -1)
                };
                return newComment;
            }
            return null;
        }

        public static void EditComment(Comment editedCom)
        {
            string request = "UPDATE comments SET Body = @body WHERE Id = @id";
            MySqlCommand cmd = new MySqlCommand(request);
            cmd.Parameters.AddWithValue("@body", editedCom.Body);
            cmd.Parameters.AddWithValue("@id", editedCom.Id);
            DbHelper hlp = new DbHelper();
            hlp.ExecuteScalar(cmd);
        }

        public static List<Comment> GetInvalidComments()
        {
            List<Comment> comList = new List<Comment>();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM comments WHERE HasBeenAccepted=0");
            DataTable coms = DbHelper.lazyExecuteQuery(cmd);
            foreach (DataRow row in coms.Rows)
            {
                comList.Add(new Comment
                {
                    Id = DbHelper.GetMapping(row, "Id", -1),
                    Body = DbHelper.GetMapping(row, "Body", string.Empty),
                    HasBeenAccepted = Convert.ToBoolean(DbHelper.GetMapping(row, "HasBeenAccepted", 0)),
                    Blog_id = DbHelper.GetMapping(row, "Blog_id", -1),
                    Author = UsersHelper.GetUserByID(DbHelper.GetMapping(row, "User_id", -1)).Name,
                    Author_id = DbHelper.GetMapping(row, "User_id", -1)
                });
            }
            return comList;
        }
    }
}