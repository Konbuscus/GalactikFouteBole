﻿using GalactikFouteBole.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GalactikFouteBole.Helpers
{
    public  class MatchHelper
    {
        /// <summary>
        /// Récupère les infos des équipes  (nom et ID) par rapport à un ID de match
        /// </summary>
        /// <param name="idMatch"></param>
        /// <returns></returns>
        public  static List<Team> GetTeamsInAMatch(int idMatch)
        {
            string request = string.Format("SELECT DISTINCT team.Id, team.Name FROM team " +
                "LEFT JOIN `match` match1  ON team.Id = match1.IdEquipe1 " +
                "LEFT JOIN `match` match2 ON team.Id = match2.IdEquipe2 " +
                "WHERE match1.IdMatch = {0} OR match2.IdMatch = {0}", idMatch);

            DbHelper DBContext = new DbHelper();
            MySqlCommand command = new MySqlCommand(request);
            DataTable Teams = DBContext.ExecuteSql(command);
            List<Team> teamList = new List<Team>();

            if(Teams != null)
            {
                foreach(DataRow row in Teams.Rows)
                {
                    Team team = new Team();
                    team.TeamId = DbHelper.GetMapping(row, "Id", 0);
                    team.TeamName = DbHelper.GetMapping(row, "Name", string.Empty);
                    teamList.Add(team);
                }
                return teamList;
            }
            return teamList;
        }

    }
}